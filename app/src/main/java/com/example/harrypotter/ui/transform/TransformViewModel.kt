package com.example.harrypotter.ui.transform

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class TransformViewModel : ViewModel() {
    val listItem = listOf("Harry","Ron","hermoine","snape","voldemort","droce")
    private val _texts = MutableLiveData<List<String>>().apply {
        value = (1..6).mapIndexed { _, i ->
            "${listItem.get(i-1)}"
        }
    }

    val texts: LiveData<List<String>> = _texts
}